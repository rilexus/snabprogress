import React from 'react';
import './style.scss';


class SnabProgress extends React.Component {
    state = {
        balkenPos: 30,
        animationName: '',
        customers: 1,
        lastDate: new Date(),
        backgroundColor: '#164151'
    };
    nameInput;


    getBalkenPos() {
        return this.state.balkenPos;
    }

    recalcBalkenPos() {

        setInterval(()=> {
            if (this.state.balkenPos < 570) {
                this.setState(state => {
                    return {
                        ...state,
                        balkenPos: this.state.balkenPos >= 500 ? 30 :  state.balkenPos + 5,
                        backgroundColor: this.state.balkenPos > 500 &&  Math.pow(-1, state.balkenPos) === 1 ? 'red': '#164151'
                    }
                })
            }
        }, 1000);
    }

    componentDidMount() {
        this.recalcBalkenPos();
        this.focusInput();
    }

    moveBalkenUp(e) {
        if (e.key === 'ArrowUp') {
            this.setState(state => {
                return {
                    ...state,
                    balkenPos: 30,
                    customers: state.customers + 1,
                    lastDate: new Date()
                }
            })
        }
    }
    focusInput() {
        this.nameInput.focus();
    }

    render() {

    return (
        <div className={''}>
            <div className={'wrapper'}>
                <div className={'snab-progress-wrapper'} style={{backgroundColor: this.state.backgroundColor}}>
                    <div className="title">Snab Progress</div>
                    <div>
                        <div className="grau-balken"></div>
                        <div className="balken" style={{top: this.getBalkenPos() + 'px'}}></div>
                        <input
                            style={{opacity: 0}}
                            onBlur={() => {this.focusInput()}}
                            ref={(input) => { this.nameInput = input; }}
                            onKeyDown={(e) => this.moveBalkenUp(e)}
                            type="text"/>
                    </div>
                </div>
                <div className="controls">
                    <div className="customers">
                        <div className="control">
                            <div className="title">
                                Total Customers
                            </div>
                            {this.state.customers}
                        </div>
                    </div>
                    <div className="control">
                        <div className="title">
                            Last Customer
                        </div>
                        <div className="date-value value">
                            {this.state.lastDate.toDateString().slice(4,11)}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
  }
}

export default SnabProgress;