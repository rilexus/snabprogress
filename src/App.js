import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import SnabProgress from "./SnabProgress/SnabProgress";

class App extends Component {
  render() {
    return (
      <div className="App">
        <SnabProgress/>
      </div>
    );
  }
}

export default App;
